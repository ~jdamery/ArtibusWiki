[[!template id=character name="Daoud bin Kelebek" house="Ex Miscellanea" job="Familiar" rank="Familiar" appage=  born=900/01/01 updated=1219/09/01 covenant="Schola Pythagoranis" parens="NA" origin="Kelebek" ethnicity="Butterfly" religion="Hedonist" age=319 image=""" [[!img Daoud size="200x" alt="Daoud"]] """]]
## Daoud bin Kelebek

----
[[Main|Daoud]] / [[Powers|Daoud/Powers]] / [[Skills|Daoud/Skills]] / [[Stats|Daoud/Stats]] / [[Additional Detail|Daoud/AdditionalDetail]]

----
[[Powers|Daoud/Powers#Powers]]

----
<a id="Powers" />
### Powers
Spell Name|Art 1|Art 2|Requisites|Level|Type|Range|Duration|Target|Description|MXP|M|Mastered Abilities|Spell Use|Sigil Manifestation
:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--:|:--:|:--|:--|:--
[[Stamp the Foot|/Meta/Spells/StamptheFoot]]|Rego|Terram| |25|Battle|Touch| | |Creates a 100 pace earthquake as per Earth Shock on p156 of core book but with range of touch| |0| | |Notes: Cost: 5, Init: +5 
[[Coerce the Djinn|/Meta/Spells/CoercetheDjinn]]|Rego|Vim| |20|Complusion| | |Individual|As Coerce the Spirits of the Night in core rulebook p 152 bu affects the Djinn| |0| | |Notes: Cost: 4, Init: +7
[[Invisible Servant|/Meta/Spells/InvisibleServant]]|Rego|Terram| |5|Utility| | |Individual|Levitates objects| |0| | |Notes: Cost: 1, Init: +13
[[Comfortable Perch|/Meta/Spells/ComfortablePerch]]|Rego|Auram| |15|Utility|Touch|Special|Individual|Wards an object or person from being touched by the rain (unlike hermetic magic, this also shelters the butterfly perching on the target but only lasts for as long as he remains still| |0| | |Notes: Cost: 2, Init: +11
[[Shining Wings|/Meta/Spells/ShiningWings]]|Creo|Ignem|Rego|10|Utility|Personal|Concentration|Individual|Creates a nimbus of light equal to a cloudy day around the butterfuly that illuminates a 15 pace area. If the ambient light is brighter, then some of it is focused on the caster so he still appears brighter than the ambient light.| |0| | |Notes: Cost: 2, Init: +11
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
&nbsp;||||||||||||||
