[[!template id=character name="Chioni" house=" " job=" " rank=" " appage=239 born=983/01/02 updated=1222/07/01 covenant="Schola Pythagoranis" parens=" " origin=" " ethnicity=" " religion=" " age=239 image=""" [[!img Chioni.jpg size="200x" alt="Chioni.jpg"]] """]]
## Chioni

----
[[Main|Chioni]] / [[Powers|Chioni/Powers]] / [[Skills|Chioni/Skills]] / [[Stats|Chioni/Stats]] / [[Additional Detail|Chioni/AdditionalDetail]]

----
[[Main Skills Chart|Chioni/Skills#Main]] | [[Other Skills Chart|Chioni/Skills#Second]] | [[General Abilities|Chioni/Skills#General]] | [[Academic Abilities|Chioni/Skills#Academic]] | [[Lores|Chioni/Skills#Lores]] | [[Crafts/Professions|Chioni/Skills#Crafts]] | [[Arcane Abilities|Chioni/Skills#Arcane]] | [[Martial Abilities|Chioni/Skills#Martial]] | [[Other Abilities|Chioni/Skills#Other]] | [[Languages|Chioni/Skills#Languages]] | [[Supernatural Abilities|Chioni/Skills#Supernatural]]

----
[[!template id=divbox content="""
<a id=Main" />
### Main Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
Animal Handling|No| |-1|3|0|-2|-6|2|1|4
Athletics|No| |0|4|1|-1|-5|3|2|5
Awareness|No| |-1|3|0|-2|-6|2|1|4
Bargain|No| |-1|3|0|-2|-6|2|1|4
Brawl|No| |-1|3|0|-2|-6|2|1|4
Carouse|No| |-1|3|0|-2|-6|2|1|4
Charm|No| |-1|3|0|-2|-6|2|1|4
Chirurgy|No| | | | | | | | | 
Concentration|No| |-1|3|0|-2|-6|2|1|4
Etiquette|No| |-1|3|0|-2|-6|2|1|4
Folk Ken|No| |-1|3|0|-2|-6|2|1|4
Guile |No| |-1|3|0|-2|-6|2|1|4
Hunt|No| |-1|3|0|-2|-6|2|1|4
Intrigue|No| |-1|3|0|-2|-6|2|1|4
Leadership|No| |-1|3|0|-2|-6|2|1|4
Legerdemain|No| | | | | | | | | 
Music|No| |-1|3|0|-2|-6|2|1|4
Ride|No| |-1|3|0|-2|-6|2|1|4
Stealth|No| |-1|3|0|-2|-6|2|1|4
Survival|No| |-1|3|0|-2|-6|2|1|4
Swim|No| |-1|3|0|-2|-6|2|1|4
Teaching|No| |-1|3|0|-2|-6|2|1|4
&nbsp;||||||||||

Artes Liberales|No| |-1|3|0|-2|-6|2|1|4
Civil and Canon Law|No| | | | | | | | | 
Common Law|No| | | | | | | | | 
Medicine|No| | | | | | | | | 
Philosophiae|No| |-1|3|0|-2|-6|2|1|4
Theology|No| | | | | | | | | 
&nbsp;||||||||||

Code of Hermes|No| | | | | | | | | 
Dominion Lore|No| | | | | | | | | 
Faerie Lore|No| | | | | | | | | 
Finesse|No| |-1|3|0|-2|-6|2|1|4
Infernal Lore|No| | | | | | | | | 
Magic Lore|No| | | | | | | | | 
Magic Theory|No| |-1|3|0|-2|-6|2|1|4
Parma Magica|No| | | | | | | | | 
Penetration|No| |-1|3|0|-2|-6|2|1|4
&nbsp;||||||||||

Bows|No| |-1|3|0|-2|-6|2|1|4
Great Weapon|No| |-1|3|0|-2|-6|2|1|4
Single Weapon|No| |-1|3|0|-2|-6|2|1|4
Thrown Weapon|No| |-1|3|0|-2|-6|2|1|4
&nbsp;||||||||||
"""]]
[[!template id=divbox content="""
<a id=Second" />
### Other Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Low German|| |0|4|1|-1|-5|3|2|5
Greek|| |0|4|1|-1|-5|3|2|5
Latin|| |-1|3|0|-2|-6|2|1|4
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Second Sight|| |-1|3|0|-2|-6|2|1|4
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="General" />
### General Abilities
:--|:--|:--
Animal Handling|Yes| 
Athletics|1| 
Awareness|0| 
Bargain|Yes| 
Brawl|0| 
Carouse|Yes| 
Charm|Yes| 
Chirurgy|No| 
Concentration|Yes| 
Etiquette|0| 
Folk Ken|0| 
Guile |0| 
Hunt|0| 
Intrigue|Yes| 
Leadership|Yes| 
Legerdemain|No| 
Music|0| 
Ride|Yes| 
Stealth|0| 
Survival|0| 
Swim|Yes| 
Teaching|0| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Arcane" />
### Arcane Abilities
:--|:--|:--
Code of Hermes|No| 
Dominion Lore|No| 
Faerie Lore|No| 
Finesse|Yes| 
Infernal Lore|No| 
Magic Lore|No| 
Magic Theory|Yes| 
Parma Magica|No| 
Penetration|Yes| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Academic" />
### Academic Abilities
:--|:--|:--
Artes Liberales|0| 
Civil and Canon Law|No| 
Common Law|No| 
Medicine|No| 
Philosophiae|0| 
Theology|No| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Martial" />
### Martial Abilities
:--|:--|:--
Bows|Yes| 
Great Weapon|Yes| 
Single Weapon|Yes| 
Thrown Weapon|Yes| 
&nbsp;||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Lores" />
### Lores
 
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Languages" />
### Languages
:--|:--|:--
Low German|1| 
Greek|1| 
Latin|0| 
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Crafts" />
### Crafts/Professions
 
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Supernatural" />
### Supernatural
:--|:--|:--
Second Sight|0| 
||
||
||
||
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Other" />
### Other Abilities
:--|:--|:--
||
||
||
||
||
&nbsp;||
"""]]
<br style="clear:left;" />
