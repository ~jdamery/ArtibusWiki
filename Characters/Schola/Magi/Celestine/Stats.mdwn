[[!template id=character name="Celestine" house=" " job=" " rank=" " appage=18 born=1215/06/15 updated=1233/09/01 covenant=" " parens=" " origin="Cambridge" ethnicity=" " religion="Catholic" age=18 ]]
## Celestine

----
[[Main|Celestine]] / [[Magic|Celestine/Magic]] / [[Skills|Celestine/Skills]] / [[Stats|Celestine/Stats]] / [[Additional Detail|Celestine/AdditionalDetail]]

----
[[Miscellaneous|Celestine/Stats#Miscellaneous]] | [[Characteristics|Celestine/Stats#Characteristics]] | [[Weapons|Celestine/Stats#Weapons]] | [[Wounds|Celestine/Stats#Wounds]] | [[Fatigue|Celestine/Stats#Fatigue]] | [[Armour|Celestine/Stats#Armour]] | [[Other Combat|Celestine/Stats#Other]]    

----
[[!template id=divbox content="""
<a id="Miscellaneous" />
### Miscellaneous
:--|:--|:--
Type|Mage| 
Gift|Gentle Gift| 
Size|0| 
Decrepitude|0| 
Warping Score|0| 
Confidence Score|1| 
Confidence Points|3| 
Faith Points|0| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Characteristics" />
### Characteristics
:--|:--
Intellect|3
Perception|1
Presence|1
Communication|1
Strength|-2
Stamina|1
Dexterity|0
Quickness|0
&nbsp;|
"""]]
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Weapons" />
### Weapons
Weapons|P?|Ability|S?|Speciality| |Initiative+dStress| |Attack+dStress| |Defense+dStress| |Base Damage+dStress|Range
:--|:--|:--|:--|:--|:--:|:--|:--:|:--|:--:|:--|:--:|:--|:--
Dodge|No|Brawl|No| |0|0+dStress|No|No|0|0+dStress|No|No|Any
Fist|No|Brawl|No| |0|0+dStress|0|0+dStress|0|0+dStress|0|-2+dStress|Melee
Kick|No|Brawl|No| |-1|-1+dStress|0|0+dStress|-1|-1+dStress|3|1+dStress|Melee
Knife|No|Brawl|No| |0|0+dStress|1|1+dStress|0|0+dStress|2|0+dStress|Melee
|||||||||||||
|||||||||||||
|||||||||||||
|||||||||||||
&nbsp;|||||||||||||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Wounds" />
### Wounds
Wound Penalty| |0
:--|:--:|:--
Light|-1| 
Medium|-3| 
Heavy|-5| 
Incapacitated|-| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Fatigue" />
### Fatigue
Fatigue Penalty|0|Fresh
:--|:--:|:--
Short Term| | 
Long Term| | 
&nbsp;|
"""]]
[[!template id=divbox content="""
<a id="Armour" />
### Armour
Armour| |Soak
:--|:--:|:--
None|0|1+dStress
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Other" />
### Other Combat
:--|:--
Load|0
Burden|0
Encumbrance|0
&nbsp;|
"""]]
<br style="clear:left;" />




 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
<br style="clear:left;" />
