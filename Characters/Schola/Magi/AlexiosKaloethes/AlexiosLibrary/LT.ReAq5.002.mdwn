> *Page of the Duck's Back*

>> Holding this sheet of vellum, beautifully illuminated with pictures of ducks and swans, and reading the poem inscribed upon it, makes the reader and his clothing immune to rain for the remainder of the day. 

>> The page has 0 penetration and can be used once per day. The magic is broken by a full immersion. 

>> _(Base 1 + 1 touch + 2 sun + 1 slightly unnatural control = 5)_
