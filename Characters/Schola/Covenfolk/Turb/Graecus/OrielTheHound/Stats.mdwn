[[!template id=character name="Oriel the Hound" house="NA" job="Turb" rank="Milites" appage=27 born=1208/03/08 updated=1236/01/01 covenant="Schola Pythagoranis" parens="NA" origin="Cambridge" ethnicity="Norman" religion="Catholic" age=27 image=""" [[!img Oriel size="200x" alt="Oriel"]] """]]
## Oriel the Hound

----
[[Main|OrielTheHound]] / [[Skills|OrielTheHound/Skills]] / [[Stats|OrielTheHound/Stats]] / [[Additional Detail|OrielTheHound/AdditionalDetail]]

----
[[Miscellaneous|OrielTheHound/Stats#Miscellaneous]] | [[Characteristics|OrielTheHound/Stats#Characteristics]] | [[Weapons|OrielTheHound/Stats#Weapons]] | [[Wounds|OrielTheHound/Stats#Wounds]] | [[Fatigue|OrielTheHound/Stats#Fatigue]] | [[Armour|OrielTheHound/Stats#Armour]] | [[Other Combat|OrielTheHound/Stats#Other]]    

----
[[!template id=divbox content="""
<a id="Miscellaneous" />
### Miscellaneous
:--|:--|:--
Type|Companion| 
Gift|Ungifted| 
Size|0| 
Decrepitude|0| 
Warping Score|0| 
Confidence Score|1| 
Confidence Points|3| 
Faith Points|0| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Characteristics" />
### Characteristics
:--|:--
Intellect|0
Perception|1
Presence|2
Communication|0
Strength|2
Stamina|1
Dexterity|1
Quickness|1
&nbsp;|
"""]]
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Weapons" />
### Weapons
Weapons|P?|Ability|S?|Speciality| |Initiative+dStress| |Attack+dStress| |Defense+dStress| |Base Damage+dStress|Range
:--|:--|:--|:--|:--|:--:|:--|:--:|:--|:--:|:--|:--:|:--|:--
Dodge|No|Brawl|No| |0|1+dStress|No|No|0|2+dStress|No|No|Any
Fist|No|Brawl|No| |0|1+dStress|0|2+dStress|0|2+dStress|0|2+dStress|Melee
Kick|No|Brawl|No| |-1|0+dStress|0|2+dStress|-1|1+dStress|3|5+dStress|Melee
Knife|No|Brawl|No| |0|1+dStress|1|3+dStress|0|2+dStress|2|4+dStress|Melee
|||||||||||||
|||||||||||||
|||||||||||||
|||||||||||||
&nbsp;|||||||||||||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Wounds" />
### Wounds
Wound Penalty| |0
:--|:--:|:--
Light|-1| 
Medium|-3| 
Heavy|-5| 
Incapacitated|-| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Fatigue" />
### Fatigue
Fatigue Penalty|0|Fresh
:--|:--:|:--
Short Term| | 
Long Term| | 
&nbsp;|
"""]]
[[!template id=divbox content="""
<a id="Armour" />
### Armour
Armour| |Soak
:--|:--:|:--
None|0|1+dStress
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Other" />
### Other Combat
:--|:--
Load|0
Burden|0
Encumbrance|0
&nbsp;|
"""]]
<br style="clear:left;" />




 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
<br style="clear:left;" />
