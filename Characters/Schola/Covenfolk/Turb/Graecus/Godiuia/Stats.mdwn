[[!template id=character name="Godiuia Silver Tongue" house="Folk Witch" job="Signifer" rank="Milites" appage=26 born=1190/06/07 updated=1216/10/13 covenant="Schola Pythagoranis" parens="Ulveva of Fleamdyke" origin="East Anglia" ethnicity="Anglo-Saxon" religion="Catholic" age=26 ]]
## Godiuia Silver Tongue

----
[[Main|Godiuia]] / [[Skills|Godiuia/Skills]] / [[Stats|Godiuia/Stats]] / [[Additional Detail|Godiuia/AdditionalDetail]]

----
[[Miscellaneous|Godiuia/Stats#Miscellaneous]] | [[Characteristics|Godiuia/Stats#Characteristics]] | [[Weapons|Godiuia/Stats#Weapons]] | [[Wounds|Godiuia/Stats#Wounds]] | [[Fatigue|Godiuia/Stats#Fatigue]] | [[Armour|Godiuia/Stats#Armour]] | [[Other Combat|Godiuia/Stats#Other]]    

----
[[!template id=divbox content="""
<a id="Miscellaneous" />
### Miscellaneous
:--|:--|:--
Type|Grog| 
Gift|Ungifted| 
Size|0| 
Decrepitude|0| 
Warping Score|0| 
Confidence Score|0| 
Confidence Points|0| 
Faith Points|0| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Characteristics" />
### Characteristics
:--|:--
Intellect|-2
Perception|2
Presence|2
Communication|2
Strength|0
Stamina|1
Dexterity|0
Quickness|0
&nbsp;|
"""]]
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Weapons" />
### Weapons
Weapons|P?|Ability|S?|Speciality| |Initiative+dStress| |Attack+dStress| |Defense+dStress| |Base Damage+dStress|Range
:--|:--|:--|:--|:--|:--:|:--|:--:|:--|:--:|:--|:--:|:--|:--
Dodge|No|Brawl|Yes|Dodging|0|-2+dStress|No|No|0|3+dStress|No|No|Any
Fist|No|Brawl|No| |0|-2+dStress|0|2+dStress|0|2+dStress|0|0+dStress|Melee
Kick|No|Brawl|No| |-1|-3+dStress|0|2+dStress|-1|1+dStress|3|3+dStress|Melee
Knife|No|Brawl|No| |0|-2+dStress|1|3+dStress|0|2+dStress|2|2+dStress|Melee
Axe|No|Single Weapon|Yes|Axe|1|-1+dStress|4|10+dStress|0|6+dStress|6|6+dStress|Melee
Axe & Shield|No|Single Weapon|Yes|Axe|1|-1+dStress|4|10+dStress|2|8+dStress|6|6+dStress|Melee
|||||||||||||
|||||||||||||
&nbsp;|||||||||||||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Wounds" />
### Wounds
Wound Penalty| |0
:--|:--:|:--
Light|-1| 
Medium|-3| 
Heavy|-5| 
Incapacitated|-| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Fatigue" />
### Fatigue
Fatigue Penalty|0|Fresh
:--|:--:|:--
Short Term| | 
Long Term| | 
&nbsp;|
"""]]
[[!template id=divbox content="""
<a id="Armour" />
### Armour
Armour| |Soak
:--|:--:|:--
None|0|1+dStress
Partial Studded|2|3+dStress
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Other" />
### Other Combat
:--|:--
Load|5
Burden|2
Encumbrance|2
&nbsp;|
"""]]
<br style="clear:left;" />




 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
<br style="clear:left;" />
