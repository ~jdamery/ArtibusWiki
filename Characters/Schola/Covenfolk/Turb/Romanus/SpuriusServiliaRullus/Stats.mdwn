[[!template id=character name="Spurius Servilia Rullus" house="NA" job="Legionary" rank="Grog" appage=26 born=1183/11/21 updated=1219/09/01 covenant="Schola Pythagoranis" parens="NA" origin="Roman Citizen" ethnicity="Greek" religion="Cult of Mithras" age=35 image=""" [[!img Spurius size="200x" alt="Spurius"]] """]]
## Spurius Servilia Rullus

----
[[Main|SpuriusServiliaRullus]] / [[Skills|SpuriusServiliaRullus/Skills]] / [[Stats|SpuriusServiliaRullus/Stats]] / [[Additional Detail|SpuriusServiliaRullus/AdditionalDetail]]

----
[[Miscellaneous|SpuriusServiliaRullus/Stats#Miscellaneous]] | [[Characteristics|SpuriusServiliaRullus/Stats#Characteristics]] | [[Weapons|SpuriusServiliaRullus/Stats#Weapons]] | [[Wounds|SpuriusServiliaRullus/Stats#Wounds]] | [[Fatigue|SpuriusServiliaRullus/Stats#Fatigue]] | [[Armour|SpuriusServiliaRullus/Stats#Armour]] | [[Other Combat|SpuriusServiliaRullus/Stats#Other]]    

----
[[!template id=divbox content="""
<a id="Miscellaneous" />
### Miscellaneous
:--|:--|:--
Type|Grog| 
Gift|Ungifted| 
Size|0| 
Decrepitude|0| 
Warping Score|0| 
Confidence Score|1| 
Confidence Points|3| 
Faith Points|0| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Characteristics" />
### Characteristics
:--|:--
Intellect|0
Perception|0
Presence|0
Communication|1
Strength|2
Stamina|2
Dexterity|0
Quickness|0
&nbsp;|
"""]]
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Weapons" />
### Weapons
Weapons|P?|Ability|S?|Speciality| |Initiative+dStress| |Attack+dStress| |Defense+dStress| |Base Damage+dStress|Range
:--|:--|:--|:--|:--|:--:|:--|:--:|:--|:--:|:--|:--:|:--|:--
Dodge|No|Brawl|No| |0|-2+dStress|No|No|0|2+dStress|No|No|Any
Fist|No|Brawl|No| |0|-2+dStress|0|2+dStress|0|2+dStress|0|2+dStress|Melee
Kick|No|Brawl|No| |-1|-3+dStress|0|2+dStress|-1|1+dStress|3|5+dStress|Melee
Knife|No|Brawl|No| |0|-2+dStress|1|3+dStress|0|2+dStress|2|4+dStress|Melee
Gladius|No|Single Weapon|Yes|Longsword|1|-1+dStress|3|9+dStress|1|7+dStress|5|7+dStress|Melee
Gladius + Scutum|No|Single Weapon|Yes|Gladius|1|-1+dStress|3|9+dStress|4|10+dStress|5|7+dStress|Melee
Pilum & Scutum|No|Single Weapon|Yes|Gladius|2|0+dStress|2|8+dStress|3|9+dStress|5|7+dStress|Melee
Pilum|No|Thrown Weapon|Yes|Pilum|0|-2+dStress|2|7+dStress|0|5+dStress|5|7+dStress|10
&nbsp;|||||||||||||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Wounds" />
### Wounds
Wound Penalty| |0
:--|:--:|:--
Light|-1| 
Medium|-3| 
Heavy|-5| 
Incapacitated|-| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Fatigue" />
### Fatigue
Fatigue Penalty|0|Fresh
:--|:--:|:--
Short Term| | 
Long Term| | 
&nbsp;|
"""]]
[[!template id=divbox content="""
<a id="Armour" />
### Armour
Armour| |Soak
:--|:--:|:--
None|0|2+dStress
Lorica|7|9+dStress
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Other" />
### Other Combat
:--|:--
Load|11
Burden|4
Encumbrance|2
&nbsp;|
"""]]
<br style="clear:left;" />




 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
<br style="clear:left;" />
