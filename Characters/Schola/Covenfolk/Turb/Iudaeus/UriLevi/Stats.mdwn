[[!template id=character name="Uri Levi" house="NA" job="Turb" rank="Immune" appage=53 born=1185/04/30 updated=1238/09/01 covenant="Schola Pythagoranis" parens="NA" origin="Cambridge" ethnicity="Jewish" religion="Jewish" age=53 image=""" [[!img Uri Levi size="200x" alt="Uri Levi"]] """]]
## Uri Levi

----
[[Main|UriLevi]] / [[Skills|UriLevi/Skills]] / [[Stats|UriLevi/Stats]] / [[Additional Detail|UriLevi/AdditionalDetail]]

----
[[Miscellaneous|UriLevi/Stats#Miscellaneous]] | [[Characteristics|UriLevi/Stats#Characteristics]] | [[Weapons|UriLevi/Stats#Weapons]] | [[Wounds|UriLevi/Stats#Wounds]] | [[Fatigue|UriLevi/Stats#Fatigue]] | [[Armour|UriLevi/Stats#Armour]] | [[Other Combat|UriLevi/Stats#Other]]    

----
[[!template id=divbox content="""
<a id="Miscellaneous" />
### Miscellaneous
:--|:--|:--
Type|Grog| 
Gift|Ungifted| 
Size|0| 
Decrepitude|1| 
Warping Score|0| 
Confidence Score|1| 
Confidence Points|3| 
Faith Points|0| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Characteristics" />
### Characteristics
:--|:--
Intellect|2
Perception|0
Presence|0
Communication|0
Strength|0
Stamina|0
Dexterity|2
Quickness|2
&nbsp;|
"""]]
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Weapons" />
### Weapons
Weapons|P?|Ability|S?|Speciality| |Initiative+dStress| |Attack+dStress| |Defense+dStress| |Base Damage+dStress|Range
:--|:--|:--|:--|:--|:--:|:--|:--:|:--|:--:|:--|:--:|:--|:--
Dodge|No|Brawl|No| |0|2+dStress|No|No|0|6+dStress|No|No|Any
Fist|No|Brawl|No| |0|2+dStress|0|6+dStress|0|6+dStress|0|0+dStress|Melee
Kick|No|Brawl|No| |-1|1+dStress|0|6+dStress|-1|5+dStress|3|3+dStress|Melee
Knife|No|Brawl|Yes|Knives|0|2+dStress|1|8+dStress|0|7+dStress|2|2+dStress|Melee
|||||||||||||
|||||||||||||
Sling|No|Thrown Weapon|Yes|Slings|-3|-1+dStress|1|11+dStress|0|10+dStress|4|4+dStress|20
|||||||||||||
&nbsp;|||||||||||||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Wounds" />
### Wounds
Wound Penalty| |0
:--|:--:|:--
Light|-1| 
Medium|-3| 
Heavy|-5| 
Incapacitated|-| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Fatigue" />
### Fatigue
Fatigue Penalty|0|Fresh
:--|:--:|:--
Short Term| | 
Long Term| | 
&nbsp;|
"""]]
[[!template id=divbox content="""
<a id="Armour" />
### Armour
Armour| |Soak
:--|:--:|:--
None|0|0+dStress
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Other" />
### Other Combat
:--|:--
Load|0
Burden|0
Encumbrance|0
&nbsp;|
"""]]
<br style="clear:left;" />




 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
<br style="clear:left;" />
