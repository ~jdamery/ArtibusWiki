#VALUE!
## Raffaelo

----
[[Main|RaffaeloInnocenti]] / [[Skills|RaffaeloInnocenti/Skills]] / [[Stats|RaffaeloInnocenti/Stats]] / [[Additional Detail|RaffaeloInnocenti/AdditionalDetail]]

----
[[Main Skills Chart|RaffaeloInnocenti/Skills#Main]] | [[Other Skills Chart|RaffaeloInnocenti/Skills#Second]] | [[General Abilities|RaffaeloInnocenti/Skills#General]] | [[Academic Abilities|RaffaeloInnocenti/Skills#Academic]] | [[Lores|RaffaeloInnocenti/Skills#Lores]] | [[Crafts/Professions|RaffaeloInnocenti/Skills#Crafts]] | [[Arcane Abilities|RaffaeloInnocenti/Skills#Arcane]] | [[Martial Abilities|RaffaeloInnocenti/Skills#Martial]] | [[Other Abilities|RaffaeloInnocenti/Skills#Other]] | [[Languages|RaffaeloInnocenti/Skills#Languages]] | [[Supernatural Abilities|RaffaeloInnocenti/Skills#Supernatural]]

----
[[!template id=divbox content="""
<a id=Main" />
### Main Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
Animal Handling|No| |2|0|1|3|-2|-2|2|0
Athletics|No|Escaping Pusuit|3|1|2|4|-1|-1|3|1
Awareness|No| |2|0|1|3|-2|-2|2|0
Bargain|Yes|Under Pressure|9|7|8|10|5|5|9|7
Brawl|No|Dodging|3|1|2|4|-1|-1|3|1
Carouse|No|Games of Chance|5|3|4|6|1|1|5|3
Charm|No|Seduction|6|4|5|7|2|2|6|4
Chirurgy|No| | | | | | | | | 
Concentration|No| |2|0|1|3|-2|-2|2|0
Etiquette|No| |2|0|1|3|-2|-2|2|0
Folk Ken|No|Motivations|4|2|3|5|0|0|4|2
Guile |No|Fast Talking|4|2|3|5|0|0|4|2
Hunt|No| |2|0|1|3|-2|-2|2|0
Intrigue|No| |2|0|1|3|-2|-2|2|0
Leadership|No| |2|0|1|3|-2|-2|2|0
Legerdemain|No| | | | | | | | | 
Music|No| |2|0|1|3|-2|-2|2|0
Ride|No| |2|0|1|3|-2|-2|2|0
Stealth|No|Urban|3|1|2|4|-1|-1|3|1
Survival|No| |2|0|1|3|-2|-2|2|0
Swim|No| |2|0|1|3|-2|-2|2|0
Teaching|No| |2|0|1|3|-2|-2|2|0
&nbsp;||||||||||

Artes Liberales|No|Astrology|9|7|8|10|5|5|9|7
Civil and Canon Law|No|Bologna|3|1|2|4|-1|-1|3|1
Common Law|No| | | | | | | | | 
Medicine|No| | | | | | | | | 
Philosophiae|No|Alchemy|8|6|7|9|4|4|8|6
Theology|No|History|3|1|2|4|-1|-1|3|1
&nbsp;||||||||||

Code of Hermes|No| | | | | | | | | 
Dominion Lore|No| | | | | | | | | 
Faerie Lore|No|Bargains|2|0|1|3|-2|-2|2|0
Finesse|No| |2|0|1|3|-2|-2|2|0
Infernal Lore|No| | | | | | | | | 
Magic Lore|No| | | | | | | | | 
Magic Theory|No| |2|0|1|3|-2|-2|2|0
Parma Magica|No| | | | | | | | | 
Penetration|No| |2|0|1|3|-2|-2|2|0
&nbsp;||||||||||

Bows|No| |2|0|1|3|-2|-2|2|0
Great Weapon|No| |2|0|1|3|-2|-2|2|0
Single Weapon|No| |2|0|1|3|-2|-2|2|0
Thrown Weapon|No| |2|0|1|3|-2|-2|2|0
&nbsp;||||||||||
"""]]
[[!template id=divbox content="""
<a id=Second" />
### Other Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
England||Cambirdge|2|0|1|3|-2|-2|2|0
Order of Hermes||Wealthy|2|0|1|3|-2|-2|2|0
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Scribe||Fair Copy|8|6|7|9|4|4|8|6
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Italian||Bologna|7|5|6|8|3|3|7|5
Latin||Written|7|5|6|8|3|3|7|5
Classical Greek||Copying|5|3|4|6|1|1|5|3
French||Paris|6|4|5|7|2|2|6|4
English||Cambridge|6|4|5|7|2|2|6|4
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Astrology Formulae| |NA - xp in here is spent on Formulae at a rate of 5xp per formula|5|3|4|6|1|1|5|3
Alchemical Formulae| |NA - xp in here is spent on Formulae at a rate of 5xp per formula|5|3|4|6|1|1|5|3
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="General" />
### General Abilities
:--|:--|:--
Animal Handling|Yes| 
Athletics|1|Escaping Pusuit
Awareness|Yes| 
Bargain|5|Under Pressure
Brawl|1|Dodging
Carouse|3|Games of Chance
Charm|4|Seduction
Chirurgy|No| 
Concentration|Yes| 
Etiquette|Yes| 
Folk Ken|2|Motivations
Guile |2|Fast Talking
Hunt|Yes| 
Intrigue|Yes| 
Leadership|Yes| 
Legerdemain|No| 
Music|Yes| 
Ride|Yes| 
Stealth|1|Urban
Survival|Yes| 
Swim|Yes| 
Teaching|Yes| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Arcane" />
### Arcane Abilities
:--|:--|:--
Code of Hermes|No| 
Dominion Lore|No| 
Faerie Lore|0|Bargains
Finesse|Yes| 
Infernal Lore|No| 
Magic Lore|No| 
Magic Theory|Yes| 
Parma Magica|No| 
Penetration|Yes| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Academic" />
### Academic Abilities
:--|:--|:--
Artes Liberales|7|Astrology
Civil and Canon Law|1|Bologna
Common Law|No| 
Medicine|No| 
Philosophiae|6|Alchemy
Theology|1|History
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Martial" />
### Martial Abilities
:--|:--|:--
Bows|Yes| 
Great Weapon|Yes| 
Single Weapon|Yes| 
Thrown Weapon|Yes| 
&nbsp;||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Lores" />
### Lores
:--|:--|:--
England|0|Cambirdge
Order of Hermes|0|Wealthy
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Languages" />
### Languages
:--|:--|:--
Italian|5|Bologna
Latin|5|Written
Classical Greek|3|Copying
French|4|Paris
English|4|Cambridge
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Crafts" />
### Crafts/Professions
:--|:--|:--
Scribe|6|Fair Copy
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Supernatural" />
### Supernatural
 
||
||
||
||
||
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Other" />
### Other Abilities
:--|:--|:--
Astrology Formulae|3|NA - xp in here is spent on Formulae at a rate of 5xp per formula
Alchemical Formulae|3|NA - xp in here is spent on Formulae at a rate of 5xp per formula
||
||
||
&nbsp;||
"""]]
<br style="clear:left;" />
