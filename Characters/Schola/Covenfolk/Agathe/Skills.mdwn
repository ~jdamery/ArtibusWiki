[[!template id=character name="Agathe" house=" " job=" " rank=" " appage=28 born=1195/05/25 updated=1223/09/01 covenant="Schola Pythagoranis" parens=" " origin="North Germany" ethnicity="Saxon" religion="Christian" age=28 image=""" [[!img Agathe.jpg size="200x" alt="Agathe.jpg"]] """]]
## Agathe

----
[[Main|Agathe]] / [[Skills|Agathe/Skills]] / [[Stats|Agathe/Stats]] / [[Additional Detail|Agathe/AdditionalDetail]]

----
[[Main Skills Chart|Agathe/Skills#Main]] | [[Other Skills Chart|Agathe/Skills#Second]] | [[General Abilities|Agathe/Skills#General]] | [[Academic Abilities|Agathe/Skills#Academic]] | [[Lores|Agathe/Skills#Lores]] | [[Crafts/Professions|Agathe/Skills#Crafts]] | [[Arcane Abilities|Agathe/Skills#Arcane]] | [[Martial Abilities|Agathe/Skills#Martial]] | [[Other Abilities|Agathe/Skills#Other]] | [[Languages|Agathe/Skills#Languages]] | [[Supernatural Abilities|Agathe/Skills#Supernatural]]

----
[[!template id=divbox content="""
<a id=Main" />
### Main Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
Animal Handling|No| |1|0|3|3|-3|-1|1|0
Athletics|No| |1|0|3|3|-3|-1|1|0
Awareness|No| |1|0|3|3|-3|-1|1|0
Bargain|No| |1|0|3|3|-3|-1|1|0
Brawl|No| |1|0|3|3|-3|-1|1|0
Carouse|No| |1|0|3|3|-3|-1|1|0
Charm|No| |1|0|3|3|-3|-1|1|0
Chirurgy|No| | | | | | | | | 
Concentration|No| |1|0|3|3|-3|-1|1|0
Etiquette|No| |1|0|3|3|-3|-1|1|0
Folk Ken|No|Ladies|2|1|4|4|-2|0|2|1
Guile |No| |1|0|3|3|-3|-1|1|0
Hunt|No| |1|0|3|3|-3|-1|1|0
Intrigue|No| |1|0|3|3|-3|-1|1|0
Leadership|No| |1|0|3|3|-3|-1|1|0
Legerdemain|No| | | | | | | | | 
Music|No|Singing|3|2|5|5|-1|1|3|2
Ride|No| |1|0|3|3|-3|-1|1|0
Stealth|No| |1|0|3|3|-3|-1|1|0
Survival|No| |1|0|3|3|-3|-1|1|0
Swim|No| |1|0|3|3|-3|-1|1|0
Teaching|No| |1|0|3|3|-3|-1|1|0
&nbsp;||||||||||

Artes Liberales|No|Geometry|6|5|8|8|2|4|6|5
Civil and Canon Law|No| | | | | | | | | 
Common Law|No| | | | | | | | | 
Medicine|No| | | | | | | | | 
Philosophiae|No|Physics|3|2|5|5|-1|1|3|2
Theology|No| | | | | | | | | 
&nbsp;||||||||||

Code of Hermes|No| | | | | | | | | 
Dominion Lore|No| | | | | | | | | 
Faerie Lore|No| | | | | | | | | 
Finesse|No| |1|0|3|3|-3|-1|1|0
Infernal Lore|No| | | | | | | | | 
Magic Lore|No| |2|1|4|4|-2|0|2|1
Magic Theory|No|Lab Work|2|1|4|4|-2|0|2|1
Parma Magica|No| | | | | | | | | 
Penetration|No| |1|0|3|3|-3|-1|1|0
&nbsp;||||||||||

Bows|No| |1|0|3|3|-3|-1|1|0
Great Weapon|No| |1|0|3|3|-3|-1|1|0
Single Weapon|No| |1|0|3|3|-3|-1|1|0
Thrown Weapon|No| |1|0|3|3|-3|-1|1|0
&nbsp;||||||||||
"""]]
[[!template id=divbox content="""
<a id=Second" />
### Other Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Low German||Saxon|6|5|8|8|2|4|6|5
Latin||Hermetic|6|5|8|8|2|4|6|5
Greek||Classical|5|4|7|7|1|3|5|4
English|| |3|2|5|5|-1|1|3|2
Romalic Greek|| |2|1|4|4|-2|0|2|1
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Enchanting Music||Singing|3|2|5|5|-1|1|3|2
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="General" />
### General Abilities
:--|:--|:--
Animal Handling|Yes| 
Athletics|Yes| 
Awareness|Yes| 
Bargain|Yes| 
Brawl|Yes| 
Carouse|Yes| 
Charm|Yes| 
Chirurgy|No| 
Concentration|Yes| 
Etiquette|Yes| 
Folk Ken|1|Ladies
Guile |Yes| 
Hunt|Yes| 
Intrigue|Yes| 
Leadership|Yes| 
Legerdemain|No| 
Music|2|Singing
Ride|Yes| 
Stealth|Yes| 
Survival|Yes| 
Swim|Yes| 
Teaching|Yes| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Arcane" />
### Arcane Abilities
:--|:--|:--
Code of Hermes|No| 
Dominion Lore|No| 
Faerie Lore|No| 
Finesse|Yes| 
Infernal Lore|No| 
Magic Lore|1| 
Magic Theory|1|Lab Work
Parma Magica|No| 
Penetration|Yes| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Academic" />
### Academic Abilities
:--|:--|:--
Artes Liberales|5|Geometry
Civil and Canon Law|No| 
Common Law|No| 
Medicine|No| 
Philosophiae|2|Physics
Theology|No| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Martial" />
### Martial Abilities
:--|:--|:--
Bows|Yes| 
Great Weapon|Yes| 
Single Weapon|Yes| 
Thrown Weapon|Yes| 
&nbsp;||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Lores" />
### Lores
 
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Languages" />
### Languages
:--|:--|:--
Low German|5|Saxon
Latin|5|Hermetic
Greek|4|Classical
English|2| 
Romalic Greek|1| 
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Crafts" />
### Crafts/Professions
 
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Supernatural" />
### Supernatural
:--|:--|:--
Enchanting Music|2|Singing
||
||
||
||
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Other" />
### Other Abilities
:--|:--|:--
||
||
||
||
||
&nbsp;||
"""]]
<br style="clear:left;" />

