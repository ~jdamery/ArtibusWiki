[[!template id=character name="Mistress Parr" house=" " job="Cat" rank=" " appage=10 born=1208/06/04 updated=1218/03/25 covenant="Schola Pythagoranis" parens=" " origin="Cambridge" ethnicity="Cat" religion="Feline" age=0 image=""" [[!img Herit-aarrt.jpg size="200x" alt="Herit-aarrt.jpg"]]
"""]]
## Mistress Parr / Herit-aarrt


----
[[Description|MistressParr#Description]] | [[Personality|Template#Personality]] | [[Qualities|Template#MundaneQualities]] | [[Virtues|Template#Virtues]] | [[Flaws|Template#Flaws]] | [[Miscellaneous|Template#Miscellaneous]] | [[Characteristics|Template#Characteristics]] | [[General|Template#General]] | [[Additional Detail|MistressParr#AdditionalDetail]]

[[!template id=divbox content="""
<a id="Description" />
### Description
:--|:--
Gender|Female
Eyes|Green
Hair|Red Tabby and White
Height|23cm
Build|Plump
Handedness|Even
&nbsp;|
"""]]
[[!template id=divbox content="""
<a id="Personality" />
### Personality Traits
:--|:--|:--
Curious|+4| 
Maternal|+2| 
Timid|+2| 
&nbsp;||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="MundaneQualities" />
### Mundane Qualities
:--|:--|:--
Ambush Predator|3|Major
Crafty|1|Minor
Good Jumper|1|Minor
Skilled Climber|1|Minor
Thick Fur|1|Minor
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Virtues" />
### Virtues
:--|:--|:--
Perfect Balance|0|Free Minor
Puissant Awareness|0|Free Minor
Sharp Ears|0|Free Minor
Puissant Leadership|1|Minor
Common Sense|1|Minor
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Flaws" />
### Flaws
:--|:--|:--
Nocturnal|0|Free Minor
Soft-hearted|1|Minor
Arthritis|1|Minor
&nbsp;||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Miscellaneous" />
### Miscellaneous
:--|:--|:--
Type|Grog| 
Gift|Ungifted| 
Size|-4| 
Decrepitude|0| 
Warping Score|0| 
Confidence Score|0| 
Confidence Points|0| 
Faith Points|0| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Characteristics" />
### Characteristics
:--|:--
Cunning      |0 
Perception   |+1
Presence     |-2
Communication|-4
Strength     |-9
Stamina      |0 
Dexterity    |+3
Quickness    |+5
&nbsp;|
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="General" />
### General Abilities
:--|:--|:--
Animal Handling|Yes| 
Athletics|3|Jumping
Awareness|4(+2)|At Night
Bargain|Yes| 
Brawl|1|Claws
Carouse|Yes| 
Charm|Yes| 
Chirurgy|No| 
Concentration|Yes| 
Etiquette|Yes| 
Folk Ken|1| 
Guile|Yes| 
Hunt|4|Mice
Intrigue|Yes| 
Leadership|1| 
Legerdemain|No| 
Music|Yes| 
Ride|Yes| 
Stealth|4|Stalking
Survival|Yes| 
Swim|Yes| 
Teaching|Yes| 
&nbsp;||
"""]]
<br style="clear:left;" />

----

<a id="Additional" />
### Additional Detail

Mistress Parr was once the most lissome and sought after of the
covenant cats and as a result she is the mother of many of them and
still rules them with a claw of steel although her body is starting to
fail and she prefers not to leave her normal haunts if she can avoid
it. The favourite of the covenant’s cook, she has gained considerable
bulk and any cat who does not show her enough respect is likely to be
held down and given a good wash. However, she is also affectionate and
a highly effective mouser who has learned to show her prowess to the
humans in the covenant kitchen. When not hunting, she can normally be
found lying on top of the bread oven.

Description: A plump cat with a reddish-orange coat and a fast
paw. She is indulgent towards attractive males but is firm in her own
strengths.
