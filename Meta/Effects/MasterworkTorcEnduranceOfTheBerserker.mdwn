Endurance of the Berserkers
---------------------------
Rego Corpus

**R**: Touch, **D**: Sun, **T**: Ind, 1 Use, Linked Trigger

The wearer's body acts as if it were unwounded and unfatigued for the
duration of the effect.  The effect will a activate based on the
Revealed Flaws effect.  Note that this effect will not be efficious
twice in succession as the body needs time to rest; hence the 1 use
per day.

----
[[Characters/Schola/Magi/AgnésKaloethes/MasterworkTorc]]
