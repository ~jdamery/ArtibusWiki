# Alas Poor History...

----

## Summary

*Johannes receives a letter asking him to investigate the loss of sections of King John's baggage train near Lynn...*

## Timing

This story ran from [[13th October 1216|Date/1216/10/13]] until [[20th October 1216|Date/1216/10/20]].

It was in sessions:

* [[Session/Senji002]]

## Cast

* Storyguide
    * [[Users/Senji]]
* Magi
    * [[Angelo|Characters/Schola/Magi/AngeloExMercere]]
    * [[Johannes|Characters/Schola/Magi/JohannesExGuernicus]]
* Consors
    * [[Gaius|Characters/Schola/Consors/GaiusValeriusCorvus]]
    * [[Maui|Characters/Schola/Consors/TomBlack]]
    * [[Robert|Characters/Schola/Consors/RobertdeDovre]]
* Grogs
    * [[Grex Romanus|Characters/Schola/Covenfolk/Turb/Romanus]]
    





## Detail

*The cast sailed for Lynn, where they picked up the trail of John's baggage train and located one of the wagons lost in the Wash; recovering a chest which contained tapestries and a pair of holy relics. They learn that the horses were spooked by a large creature, which members of the baggage train describe as either a large dog or a large bird. Following the train leads them to King John at Newark, where he is dying. They learn that his food taster died previously and there is speculation he was poisoned by/with a frog; but they are no closer to learning if magic was used to cause either John's illness or the losses to his baggage train. Feeling out of choices, Johannes agrees for Angelo to attempt to heal the King, in the hope that it will provoke whatever hostile forces exist to strike again...*

## Effects

All Characters: 10 Source Quality
:   Party unravelled some of the plot, and healed John of the poison.
