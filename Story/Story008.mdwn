# *The Tribunal of 1222*

----

## Summary

*The Junior Mages attended the Tribunal of 1222, hosted at Blackthorn covenant. They managed to steer a sufficiently middle course between the Tribunals political titans, Julia Ex Jerbiton and Goliard Ex Tremere not to offend anybody and resolved a local dispute relating to East Anglian vis sources*

## Timing

This story ran at various points from [[October 1221|Date/1221/10]] until [[September 1222|Date/1222/09]]. 

It was in sessions:

* [[Session/Ilanin001]]
* [[Session/Ilanin002]]
* [[Session/Ilanin003]]

## Cast

* Storyguide
    * [[Ilanin]]
* Magi
    * [[Alexios|Characters/Schola/Magi/AlexiosKaloethes]]
    * [[Angelo|Characters/Schola/Magi/AngeloExMercere]]
    * [[Johannes|Characters/Schola/Magi/JohannesExGuernicus]]
    * [[Tanis|Characters/Schola/Magi/TanisKatsakisTanithou]]
* Grogs
    * [[Grex Celtae|Characters/Schola/Covenfolk/Turb/Celtae]]
    
## Detail

* Tribunal attendance:
   * Talion Ex Flambeau (praeco)
   * Iudicum Ex Guernics (presiding Quaesitor) 
   * Goliard Ex Tremere (with the sigils of Prelum, Fornax, Justin and Caltis Ex Tremere)
   * Golias Ex Tytalus
   * Caitlin Ex Miscellanea
   * Scintilla Ex Flambeau and her newly gauntleted apprentice
   * Trutina Ex Guernics
   * Gwryhr Ex Miscellanea
   * Findabair Ex Miscellanea
   * Gregorius Ex Bonisagus (with the sigils of Lucidia and Jonaquil)
   * Fabricor Ex Verditius 
   * Siffed Ex Criamon (with the sigil of Ariel)
   * Herrit Ex Tytalus (with the sigil of Thamik)
   * Alexios Ex Miscellanea
   * Johannes Ex Guernics
   * Angelo Ex Mercere
   * Tanis Ex Merinitia
   * Flavius Ex Miscellanea
   * Kirist Ex Flambeau
   * Julia Ex Jerbiton (with the sigils of Corvus, Phessalia and Desiderius)
   * Cremate Spurios Ex Flambeau
   * Timor Ex Guernicus

* Issues: 
   * Should the Tribunal establish a meeting place upon the piece of land outside London offered by Julia for this purpose, working in partnership with the London Guild of Pepperers who will maintain the House when the Order does not need it. 
       * _Held_: Yes. Julia Ex Jerbiton is ordered to oversee the creation of a suitable building. 
   * Does a hoplite violate the Code if he slays a renounced magus immediately upon their renunciation, thereby preventing that magus's sodales from seeking him out and slaying them as requested by the Hermetic Oath?
       * _Held_: No, but given the Order's traditions on the awarding of a renounced magus's posessions to their killer, such an action does constitute deprivation of magical power. Given the circumstances of mitigation in this case, a fairly small vis fine was levied on the accused (Cremate Spurious), which the Praeco promptly increased when the defendant implied this would not stop him doing the same act again. 
   * Should a prohibition be enacted on the establishment of new Covenants within 75 miles of each other?
       * _Held_: Yes; Johannes proposed and argued in favour of this motion, convincing much of the Tribunal that it was an important measure to ensure that conflict over resources did not cause the Tribunal to erupt in warfare again. 
   * Subject to the above, does the Tribunal bless the foundation of a new covenant by petitioners from the Normandy Tribunal?
       * _Held_: Yes, this was a formality. The covenant will be established at or near to Winchester by three Jerbitons and a Merinitia. 
   * Does a member of the order scry upon another member if he learns of their activities through reading the mind of a mortal with whom he did not know that the second member had dealings with?
       * _Held_: No. This is a somewhat contentious position, and a certain number of bribes were flying around to purchase votes in both directions. The Tremere appeared to want to start rolling back the expansive definition of scrying the order has adopted, starting here; this was successful. 
   * Where may the covenants of Schola Pythagoranis, Libellus, Nigrasaxa and Winchester prospect for new vis sources without interfering with each others' reasonable claims and thereby depriving each other of magical power?
       * _Held_: A compromise proposal was adopted, adjusted slightly after Gregorius objected on behalf of Libellus that the heavily populated regions around London would almost certainly contain fewer vis sources than the more rural regions. The area belonging to the Schola mostly extends North from Cambridge in a cone which includes all of the known Order of the Spheres sites. 
   * Should the Tribunal disregard proxy votes from magi who have not presented themselves at any of the last three Tribunals?
       * _Held_: Yes. This proposal from Goliard was somewhat controversial, but attracted support from mages wanting to avoid the issues the Rhine tribunal has with proxy votes from dead magi being commonplace. The majority of magi present were convinced by Goliard's contention that it was unlikely that any magus could be so busy as to be unable to take two weeks off in twenty-one years. 
   * Does the Tribunal recognise that the Schola Pythagoranis holds the isle of Enys Samson in trust for the Covenant of Stellasper, should they return and wish to claim it?
       * _Held_: Yes. Goliard was somewhat unhappy about this but lacked effective means of opposing it. She appeared not to have been informed which was interesting because other Tremere appeared to know. 

##Other Things 

Findabair approached Johannes about some of the troubles at Cad Gadu (see Correspondences).

Flavius approached Alexios about potentially investigating the mysteries of some of the more esoteric enchanted items at Ungulus, and asked Johannes if he would consent to participate in a scheme to allow him to gain a legal ruling which would force freer excess to Ungulus (and probably cause several of the crazy older magi to leave). Johannes declined for ethical reasons. 

Alexios asked Siffed about an academic Criamon with whom he might be able to correspond and who also may know about ancient Middle Eastern languages; Siffed suggested Alden Ex Criamon (see Correspondences).

The Schola's junior magi invited the young magi of other covenants to work with them on unravelling the mystery of the Order of the Spheres; furthermore they agreed with Libellus and Nigrasaxa that in principle magi from each covenant should be able to study in the libraries of the other two. 

