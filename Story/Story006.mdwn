# *For this my Daughter was Dead, and is Alive Again*

----

## Summary

*Alexios lead a trip to Barcelona and beyond following the trail of
 his daughter; whom it was found was now a blacksmith on the border
 between Spain and France*

## Timing

This story ran from [[16th September 1219|Date/1219/09/16]] until
[[24th September 1219|Date/1219/09/24]].

It was in sessions:

* [[Session/Senji006]]

## Cast

* Storyguide
    * [[Users/Senji]]
* Magi
    * [[Alexios|Characters/Schola/Magi/AlexiosKaloethes]]
    * [[Angelo|Characters/Schola/Magi/AngeloExMercere]]
    * [[Johannes|Characters/Schola/Magi/JohannesExGuernicus]]
    * [[Tanis|Characters/Schola/Magi/TanisKatsakisTanithou]]
* Consors
    * [[Gaius|Characters/Schola/Consors/GaiusValeriusCorvus]]
    * [[Maui|Characters/Schola/Consors/TomBlack]]
* Grogs
    * Some shieldgrogs
    
## Detail

*Detailed description of events goes here.*
