<TMPL_IF name>
<div class="infobox">
Name: <TMPL_VAR name><br />
<TMPL_IF author>Author: <TMPL_VAR author><br /></TMPL_IF>
<TMPL_IF lang>Language: <TMPL_VAR lang><br /></TMPL_IF>
<hr />
<TMPL_IF subject>Subject: <TMPL_VAR subject><br /></TMPL_IF>
<TMPL_IF level>Level: <TMPL_VAR level><br /></TMPL_IF>
<TMPL_IF quality>Quality: <TMPL_VAR quality><br /></TMPL_IF>
<hr />
<TMPL_IF scribe>Skilled Scribe: <TMPL_VAR scribe><br /></TMPL_IF>
<TMPL_IF binder>Skilled Binder: <TMPL_VAR binder><br /></TMPL_IF>
<TMPL_IF illumination>Skilled Illumination: <TMPL_VAR illumination><br /></TMPL_IF>
<TMPL_IF resonant>Mystical Resonance: <TMPL_VAR resonant><br /></TMPL_IF>
<TMPL_IF translated>Translated From: <TMPL_VAR translated><br /></TMPL_IF>
<TMPL_IF copyright><hr />
Oath: <TMPL_VAR oath><br />
© <TMPL_VAR copyright><br /></TMPL_IF>
</div>
<TMPL_ELSE>
This template is used to create an infobox for a character.  It uses
these parameters:

<ul>
<li>name - the name of the book
<li>author - the author of the book (optional)
<li>lang - the language of the book (optional)
<li>subject - the subject of the book (optional)
<li>level - the level of the book (optional)
<li>quality - the quality of the book (optional)
<li>scribe - is the book written by a skilled scribe (optional)
<li>binder - was the book bound by a skilled binder (optional)
<li>illumination - was the book illuminated by a skilled illuminator (optional)
<li>resonant - what resonant effects apply (optional)
<li>translated - what language was this book translated from (optional)
<li>copyright, oath - copyright source and appropriate oath (optional)
</ul>
</TMPL_IF>
