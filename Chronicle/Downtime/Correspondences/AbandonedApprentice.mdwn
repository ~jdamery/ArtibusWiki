From Johannes, Summer 1224

> To Iudicium ex Guernicus, Senior Quaesitor of the Stonehenge Tribunal, Johannes sends respectful greetings:
> 
> My sodales and I recently chanced to discover in the pursuit of other goals
> that a Gifted individual had been living in secret some few miles downriver from 
> Cambridge.
>
> The shocking aspect of this encounter was that the individual in question was
> protected by something that seemed like, and turned out to be, the Parma Magica.
>
> It transpired that they had once been an Hermetic apprentice, but that their
> parens had cast them forth with their training incomplete - and had most foolishly
> allowed them to learn the Parma before they did this!
> 
> The apprentice in question is not of sweet disposition, but did most willingly
> swear the Oath in my presence and that of my sodales, and has pledged himself
> to abide by its tenets; it is clear that he needs to be admitted into one of
> the Houses, but now past the crisis, I must confess that I do not know the 
> correct protocol and procedures to follow. 
> 
> I enclose a full report on the matter, which I hope you will be able to 
> appropriately pursue.
>
> Yours with respect,
>
> Johannes ex Guernicus
