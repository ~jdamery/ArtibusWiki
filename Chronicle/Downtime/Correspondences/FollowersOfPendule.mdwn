From Tanis, Autumn 1218, to Handri & Vinaria of Irencillia & Caitlin
Suil Uaine of Crun Clach (three seperate letters, all identical)

> Blessings be upon you,
> 
> I'm sure you will have heard of my parens, Elly of Animus Silvae in
> Novgorod.
>
> [Long description of events following Tanis' apprenticeship, and the vanishing of Elly into Arcadia]
>
> So I find myself in a position of having noone of my Society to
> guide me.  Do you know of any other of the Followers of Pendule who
> reside in Western Europe; preferably within these isles of Alwion
> and Iwernia; who might be willing to take me under their wing as an
> adoptive protégé?
>
> Yours in friendship,
>
> Tanis filia Elly, of the Followers of Pendule.

