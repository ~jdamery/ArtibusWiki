## Date

* 17th October 2015

## Stories

* [[Story/Template]]

## Setup

*Description of the state of the story, or the prerequisites for the
 session go here*
 
## Players should ensure they have

* Magi characters readyish

## Attendance

* [[Users/Ilanin]]
* [[Users/Senji]] (Storyguide)
* [[Users/Shadow]]
* [[Users/Zavier]] (via Skype)
