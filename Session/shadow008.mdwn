## Date

* 21st May 2016

## Stories

* [[Where Many Paths and Errands Meet|Story/Story022]]

## Setup

*Description of the state of the story, or the prerequisites for the
 session go here*
 
## Players should ensure they have

* Magi characters readyish

## Attendance

* [[Users/Ilanin]]
* [[Users/Senji]] 
* [[Users/Shadow]] (Storyguide)
* [[Users/Zavier]] (via Skype)
