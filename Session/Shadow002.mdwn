## Date

* 7th November 2015

## Stories

* [[Something hidden. Go and find it...|Story/Story003]] ([[And a brimming stream ran by|Story/Story003/Chapter002]])

## Setup

*The Magi had previously found an underground complex, including an orrery and a mithraeum, on the Isle of Eels.*

*The session will start with the discussion of when and how to continue the investigation, and will probably include investigating Earith.*
 
## Players should ensure they have

* The characters they are planning to bring ready!
* Some idea of their characters downtime plans. 

## Attendance

* [[Users/Ilanin]]
* [[Users/Senji]]
* [[Users/Shadow]] (Storyguide)
* [[Users/Zavier]] (via Skype)
