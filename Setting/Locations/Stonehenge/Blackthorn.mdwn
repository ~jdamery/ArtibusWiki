Blackthorn is sited in caves under the Black Mountains in south Wales.

The caves were originally excavated by Tagelyn covenant, but
Blackthorn evicted them and took them over.  The magi of Tagelyn fled
to the Hibernian tribunal and set up the Ashenrise covenant.

Blackthorn is a powerful Tremere-led covenant.  They no-longer have
complete control of the tribunal, but they're close.  They have the
praeco and hence host the tribunal.

* Talion of Flambeau (praeco)
* Iudicium of Guernicus (senior quaesitor)
* Golias of Tytalus
* Goliard of Tremere (apparent leader)
* Fornax of Tremere 
* Prelum of Tremere

