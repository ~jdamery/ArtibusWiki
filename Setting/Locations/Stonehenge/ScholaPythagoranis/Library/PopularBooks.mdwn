### [[Popular Book Use List|PopularBooks]]

Book                   | Sp1219 | Su1219 | Au1219 | Wi1219 | Sp1220 | Su1220 | Au1220 | Wi1220 | Sp1221 | Su1221 | Au1221 | Wi1221 | Sp1222 | Su1222 | Au1222 | Wi1222 |
:----------------------|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|
***In Complexi…***     |        | Gaius  |        |        | Tanis  | (Esk)  |        | Gaius  |        |        |        |        |        | Maui  |        |        |
***Diary***            |  N/A   |  N/A   |        |        | (Esk)  | Tanis  |        |        |        | Gaius  |        | Gaius  |        |        |        |        |
***Improvements to…*** |  N/A   |  N/A   | Tanis  | Daoud  |        | Daoud  | Daoud  | Daoud  |        |        |        |        |        |        |        |StudyGp |
***Health and True…*** |        |        |        |        |        |        |        |        |Angelo  |Angelo  |        |        |        |Alexios |        |        |
***Mysteria Magica***  |        |        |        |        |        |        |        |        |Johannes|        |        |        |        |        |        |        |
***Auctoritatemque…*** |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |
&nbsp;|||||||||||||


Book                   | Sp1223 | Su1223 | Au1223 | Wi1223 | Sp1224 | Su1224 | Au1224 | Wi1224 | Sp1225 | Su1225 | Au1225 | Wi1225 | Sp1226 | Su1226 | Au1226 | Wi1226 |
:----------------------|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|
***In Complexi…***     |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |
***Diary***            |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |
***Improvements to…*** |        |        |        |StudyGp |        |        |        |StudyGp |        |        |        |        |        |        |        |        |
***Health and True…*** | Angelo |        |        |        | Angelo | Alexios  | Alexios  |        |        | Alexios |        |        |        |        |        |        |
***Mysteria Magica***  | Tanis  | Tanis  |Johannes|        |        |        |        |        |        |        |        |        |        |        |        |        |
***Auctoritatemque…*** |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |
&nbsp;|||||||||||||
