[[!template id=book name="Η Κατάρα του Απόλλωνα" author="Cassandra of Troy" lang="Classical Greek" subject="Premonitions" level="2" quality="9" scribe="yes" binder="yes" illumination="yes"]]

Written on one dense papyrus scroll this is the closest thing to an
authority available to those who suffer from premonitions.

No advice is given on getting people to pay attention to the
premonitions.
