The Terram lab is in a natural cavern accessible off one of the worked
out mine drifts.  It is approximately 900sqft (Size +2) and the floor
is uneven and full of holes and pits all of which make it a rather
inconvenient location for a lab (Uneven Floor, Greater Feature,
Greater Focus, Awkward Shape). One of the pits is a well (Lesser Feature).  The walls
of the cavern have been enchanted to provide lighting, which can be
adjusted by the maga to their desire (Magical Lighting -- Excessive or
Superior as desired).  It is impossible to keep properly dry (Damp),
but a natural flow of air through the caverns permits a large fire to
be used to keep the whole cavern warm (Superior Heating).

There is a collection of about three dozen different kinds of rock,
some of which are large enough to be used as pieces of furnature
(Specimens, also counts as a Realia of Quality 5).

The one dry sections; a slightly raised blister in the wall; is devoted
to a large supply cupboard (Extensive Stores)

There is more rock than you can possibly imagine (Inexhaustible Supplies)

| Size |                   | GQ | Up | Safe | Warp | Heal | Æsth | Te | Im | Aq | Ig | Texts | Vis Ext | Items |
|-----:|:------------------|:--:|:--:|:----:|:----:|:----:|:----:|:--:|:--:|:--:|:--:|:-----:|:-------:|:-----:|
|    0 | Uneven Floor      |    |    |   -1 |      |   -1 |   -1 | +1 |    |    |    |       |         |       |
|   +3 | Greater Feature   |    |    |      |      |      |   +3 | +3 |    |    |    |       |         |       |
|   -3 | Greater Focus     | -2 |    |      |      |      |      | +4 |    |    |    |       |         |       |
|   -1 | Awkward Shape     |    |    |   -2 |      |      |   -1 |    |    |    |    |       |         |       |
|   +1 | Lesser Feature    |    |    |      |      |      |   +1 |    |    | +1 |    |       |         |       |
|    0 | Magical Lighting* |    |    |      |      |      |   +1 |    | +1 |    |    |    +1 |         |       |
|   +1 | Superior Heating  |    | +1 |      |      |   +1 |   +1 |    |    |    | +1 |       |         |       |
|   -1 | Damp              |    | +1 |      |      |   -1 |   -1 |    |    | +1 |    |       |         |       |
|   +1 | Specimens         |    | +1 |      |      |      |   +1 | +1 |    |    |    |       |         |       |
|   +1 | Extensive Stores  |    |    |   +2 |      |      |      |    |    |    |    |       |         |       |
|    0 | Inexhaustible Supplies | |-3|      |  +1  |      |      |    |    |    |    |       |         |       |
|    0 | Servant (Int 1)   |    |    |   +1 |      |      |   +1 |    |    |    | +1 |       |         |       |
|    0 | Superior Equipment| +1 | +2 |   +1 |      |      |      |    |    |    |    |       |   +1    |       |
|    0 | Superior Tools    |    | +1 |   +1 |      |      |      |    |    |    |    |       |         |    +1 |
|   0  | Base Safety (Ref - OccSz) | | | -2 |      |      |      |    |    |    |    |       |         |       |
 &nbsp; |||||||||||||

|   +2 | **Totals**        | -2 | +3 |    0 |      |   -1 |   +4 | +9 | +1 | +2 | +1 |    +1 |   +1    |   +1  |
| &nbsp; |||||||||||||
[Lab Details]  

* Stats given are for Superior Lighting

|             |Score|
|------------:|:---:|
|   Size      | +2  |
|Virtue Points|  7  |
|Flaw Points  |  5  |
|Refinement   |  0  |
|Occupied Size| +2  |
|Free points  |  0  |
[Lab Size]
