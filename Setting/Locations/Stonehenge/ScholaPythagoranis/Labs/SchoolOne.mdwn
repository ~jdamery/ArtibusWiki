In the School

| Size |                                                   | GQ | Up | Safe | Warp | Heal | Æsth |Teach|
|-----:|:--------------------------------------------------|:--:|:--:|:----:|:----:|:----:|:----:|:--:|
|   +1 | Pulpit                                            |    |    |      |      |      |   +1 | +1 |
|   +1 | Spacious                                          |    |    |  +2  |      |      |   +1 |    |
|   -3 | Elementary (Teaching)                             | -2 | -3 |      |      |      |      |    |
| &nbsp; |||||||||

|   -1 | **Totals**                                        | -2 | -3 |  +2  |      |      |   +2 | +1 |
| &nbsp; |||||||||
[Lab Details]
