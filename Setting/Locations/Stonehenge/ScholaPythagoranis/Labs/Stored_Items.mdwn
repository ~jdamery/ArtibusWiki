Here are stored items that the new magi removed from the unmaintaned labs. These include:

* A large limestone sarcophagus, undecorated.  Inside it is the skeleton of a young woman.

| Size |                    | GQ | Up | Safe | Warp | Heal | Æsth | Vis Ex | Pe | Co | 
|-----:|:-------------------|:--:|:--:|:----:|:----:|:----:|:----:|:------:|:--:|:--:|
|   +1 | Sarcophagus        |    |    |      |      |      |   +1 |        | +1 |    |

Also items found on adventures:

* 3 Chests found in Stellasper with the arms of the Roma Nova covenant
* <del>Enough good quality glass to add "Superior Equipment" to one lab (but not pay the upkeep)</del> - Angelo gave Celetine permission to use this
* A chest of glassware, with enough good quality glass to add "Superior Equipment" to one lab (but not pay the upkeep)
* A large metal map of the stars, inlaid with gems
* Three ceramic cubes, big, medium and small
* A large circular metal mirror
