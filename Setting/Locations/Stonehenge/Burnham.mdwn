Burnham is in the far north.  It is sited in a castle and is mostly
reclusive.  They send one representative to Tribunal with all their
proxies and mostly stay out of politics.

* Aindread Ex Miscellanea (apparent leader)
* Caitlin Ex Miscellanea
* Scintilla of Flambeau
* Focus of Flambeau
* Sachiko of Merinita
* Trutina the Quaesitor
